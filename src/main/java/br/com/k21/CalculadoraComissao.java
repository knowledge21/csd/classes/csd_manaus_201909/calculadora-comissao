package br.com.k21;

import java.math.BigDecimal;

public class CalculadoraComissao {

	public static double Calcular(double venda) {
		double resultado = venda * 5 / 100;

		if (venda > 10000)
			resultado = venda * 6 / 100;

		return Math.floor(resultado * 100) / 100;

	}

	public static int Calcular(int venda) {

		if (venda > 10000)
			return venda * 6 / 100;

		return venda * 5 / 100;

	}
	
	public static BigDecimal Calcular(BigDecimal venda) {
		if (venda.compareTo(new BigDecimal("10000"))  >= 0)
			return venda.multiply(new BigDecimal("6")).divide(new BigDecimal("100"));
		return venda.multiply(new BigDecimal("5")).divide(new BigDecimal("100"));
	}


}

